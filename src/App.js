import React, { Component } from 'react';
import firebase from 'firebase';
import ChatWindow from './ChatWindow';
import TextInput from './TextInput';
import './App.css';

class App extends Component {
  constructor(){
    super();
    this.state = {
      messages: null
    }
  }

  componentWillMount () {
    const rootRef = firebase.database().ref('messages');
    rootRef.on('value', (db)=> {
        this.setState({
            messages: db.val()
          })
      })
    }

  sendMessage (message, name, time) {
    const rootRef = firebase.database().ref('messages').push({
        message: message,
        name: name,
        time: time
    })
  }
  render() {
    return (
      <div className="section">
          <ChatWindow messages={this.state.messages} />
          <TextInput sendMessage={this.sendMessage} />
      </div>
    );
  }
}

export default App;
