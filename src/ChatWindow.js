import React from 'react';

const ChatWindow = ({messages}) => {


    const renderMessage = (value) => {
        const item = value[1];
        return(
            <div className="message">
                <div className="avatar">{item.name.charAt(0)}</div>
                <div className="info">
                <div className="date">{item.time}</div>
                <div className="username">{item.name}</div>
                <div className="text">
                    <p>{item.message}</p>
                </div>
                </div>
            </div>
        )
    }
    const items = messages && Object.entries(messages);
    return (
        <div className="messages">
            {
                items && items.map(item => {
                    return renderMessage(item)
                }) || "Загружаем сообщения..."
            }
            
        </div>
    );
}

export default ChatWindow;