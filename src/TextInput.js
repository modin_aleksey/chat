import React from 'react';
import moment from 'moment';

const TextInput = ({sendMessage}) => {
    const name = "Пётр";
    let message;
    const sendNewMessage = () => {
        const date = moment().format('Отправлено DD.MM в HH:mm');
        sendMessage(message.value, name, date);
        message.value = "";
    }
    return (
        <div className="form_place">
         <div className="new_message_form">
               <div className="textarea">
                   <textarea
                        rows="1" 
                        ref={el => message = el}
                    >
                   </textarea>
                </div>
               <button 
                    onClick={()=>sendNewMessage()}
                    className="button btn-blue"
                    >Написать
               </button>
         </div>
      </div>
    )
}

export default TextInput;