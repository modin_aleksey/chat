import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { initializeApp } from 'firebase';
import { Provider } from 'react-firebase';

const config = initializeApp({
    apiKey: "AIzaSyCBEXUEnKsybOSsojPlYTBS88TbbgMOmSI",
    authDomain: "buzztest-e75b8.firebaseapp.com",
    databaseURL: "https://buzztest-e75b8.firebaseio.com",
    projectId: "buzztest-e75b8",
    storageBucket: "buzztest-e75b8.appspot.com",
    messagingSenderId: "928354657436"
});

ReactDOM.render(
    <Provider firebaseApp={config}>
        <App/>
    </Provider>, 
document.getElementById('root'));
